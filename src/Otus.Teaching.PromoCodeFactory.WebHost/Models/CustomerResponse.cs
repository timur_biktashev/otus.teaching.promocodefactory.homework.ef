﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class CustomerResponse
    {
        /// <summary>
        /// Идентификатор клиента
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Адрес почты
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Список предпочтений
        /// </summary>
        public List<PreferenceResponse> Preferences { get; set; }

        /// <summary>
        /// Список промокодов
        /// </summary>
        public List<PromoCodeShortResponse> PromoCodes { get; set; }

        public CustomerResponse(Customer customer)
        {
            Id = customer.Id;
            FirstName = customer.FirstName;
            LastName = customer.LastName;
            Email = customer.Email;
            Preferences = customer.Preferences?
                .Where(x => x.Preference != null)
                .Select(x => new PreferenceResponse(x.Preference))
                .ToList();
            PromoCodes = customer.PromoCodes?
                .Select(x => new PromoCodeShortResponse(x.PromoCode))
                .ToList();
        }
    }
}