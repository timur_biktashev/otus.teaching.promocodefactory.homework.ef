﻿using System;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    /// <summary>
    /// Информация о промокоде
    /// </summary>
    public class PromoCodeShortResponse
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public Guid Id { get; set; }
        
        /// <summary>
        /// Код
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Сервисная информация
        /// </summary>
        public string ServiceInfo { get; set; }

        /// <summary>
        /// Дата начала
        /// </summary>
        public string BeginDate { get; set; }

        /// <summary>
        /// Дата окончания
        /// </summary>
        public string EndDate { get; set; }

        /// <summary>
        /// Партнер
        /// </summary>
        public string PartnerName { get; set; }

        public PromoCodeShortResponse()
        {
        }

        public PromoCodeShortResponse(PromoCode promoCode)
        {
            Id = promoCode.Id;
            Code = promoCode.Code;
            ServiceInfo = promoCode.ServiceInfo;
            BeginDate = promoCode.BeginDate.ToString();
            EndDate = promoCode.EndDate.ToString();
            PartnerName = promoCode.PartnerName;
        }
    }
}