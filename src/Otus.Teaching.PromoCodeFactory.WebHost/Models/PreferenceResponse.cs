﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    /// <summary>
    /// Информация о предпочтении
    /// </summary>
    public class PreferenceResponse
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }

        public PreferenceResponse()
        {
        }

        public PreferenceResponse(Preference preference)
        {            
            Id = preference.Id;
            Name = preference.Name;
        }
    }
}
