﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    /// <summary>
    /// Создать или обновить информацию по клиенту
    /// </summary>
    public class CreateOrEditCustomerRequest
    {
        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Адрес почты
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Список идентификаторов предпочтений
        /// </summary>
        public List<Guid> PreferenceIds { get; set; }
    }
}