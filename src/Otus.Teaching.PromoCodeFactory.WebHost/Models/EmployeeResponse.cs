﻿using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    /// <summary>
    /// Информация о сотруднике
    /// </summary>
    public class EmployeeResponse
    {
        /// <summary>
        /// Идентифкатор сотрудника
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// ФИО
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Адрес почты
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Роль
        /// </summary>
        public RoleItemResponse Role { get; set; }

        /// <summary>
        /// Количество промокодов
        /// </summary>
        public int AppliedPromocodesCount { get; set; }
    }
}