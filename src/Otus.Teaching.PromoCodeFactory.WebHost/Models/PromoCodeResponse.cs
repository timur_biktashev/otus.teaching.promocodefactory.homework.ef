﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    /// <summary>
    /// Информация о промокоде
    /// </summary>
    public class PromoCodeResponse
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Код
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Сервисная информация
        /// </summary>
        public string ServiceInfo { get; set; }

        /// <summary>
        /// Дата начала
        /// </summary>
        public string BeginDate { get; set; }

        /// <summary>
        /// Дата окончания
        /// </summary>
        public string EndDate { get; set; }

        /// <summary>
        /// Имя партнера
        /// </summary>
        public string PartnerName { get; set; }

        public List<CustomerShortResponse> Customers { get; set; }

        public PromoCodeResponse()
        {
        }

        public PromoCodeResponse(PromoCode promoCode)
        {
            Id = promoCode.Id;
            Code = promoCode.Code;
            ServiceInfo = promoCode.ServiceInfo;
            BeginDate = promoCode.BeginDate.ToString();
            EndDate = promoCode.EndDate.ToString();
            PartnerName = promoCode.PartnerName;
            Customers = promoCode.Customers?
                .Select(x => new CustomerShortResponse(x.Customer))
                .ToList();
        }
    }
}
