﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> customerRepository;
        private readonly IRepository<Preference> preferenceRepository;

        public CustomersController(IRepository<Customer> customerRepository,
                                   IRepository<Preference> preferenceRepository)
        {
            this.customerRepository = customerRepository;
            this.preferenceRepository = preferenceRepository;

        }

        /// <summary>
        /// Получить данные всех клиентов
        /// </summary>
        /// <returns>данные клиентов</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CustomerShortResponse>>> GetCustomersAsync()
        {
            var customers = await customerRepository.GetAllAsync();
            if(customers == null || !customers.Any())
            {
                return NotFound();
            }
            return Ok(customers.Select(s => new CustomerShortResponse(s)).ToList());
        }
        
        /// <summary>
        /// Получить данные по клиенту
        /// </summary>
        /// <param name="id">Идентификатор клиента</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var result = await customerRepository.GetByIdAsync(id);
            if(result == null)
            {
                return NotFound();
            }
            return Ok(new CustomerResponse(result));
        }
        
        /// <summary>
        /// Создать клиента
        /// </summary>
        /// <param name="request">Информация о клиенте</param>
        [HttpPost]
        public async Task<ActionResult<CustomerResponse>> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            if (request == null)
            {
                return BadRequest();
            }

            var preferences = await GetPreferencesByIdsAsync(request.PreferenceIds);
            var customer = CustomerMapper(request, preferences);

            await customerRepository.CreateAsync(customer);

            var response = new CustomerResponse(customer);
            return CreatedAtRoute(null, response);
        }

        /// <summary>
        /// Обновить данные клиента
        /// </summary>
        /// <param name="id">Идентификатор клиента</param>
        /// <param name="request">Информация для обновления</param>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            if (request == null)
            {
                return BadRequest();
            }

            var customer = await customerRepository.GetByIdAsync(id);
            if (customer == null)
            {
                return NotFound();
            }

            var preferences = await GetPreferencesByIdsAsync(request.PreferenceIds);
            CustomerMapper(request, preferences, customer);

            await customerRepository.UpdateAsync(customer);

            return NoContent();
        }
        
        /// <summary>
        /// Удалить клиента
        /// </summary>
        /// <param name="id">Идентификатор клиента</param>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await customerRepository.GetByIdAsync(id);
            if (customer == null)
            {
                return NotFound();
            }

            await customerRepository.DeleteByIdAsync(id);
            return Ok();
        }

        private async Task<IEnumerable<Preference>> GetPreferencesByIdsAsync(IEnumerable<Guid> ids)
        {
            if(ids?.Count() < 1) //или через Any(), интересно, как красиво перевести к bool из bool?
            {
                return await Task.FromResult(Enumerable.Empty<Preference>());
            }
            return await preferenceRepository.GetRangeByIdsAsync(ids.ToList());
        }

        private Customer CustomerMapper(CreateOrEditCustomerRequest request,
                                        IEnumerable<Preference> preferences,
                                        Customer customer = null)
        {
            if (customer == null)
            {
                customer = new Customer
                {
                    Preferences = new List<CustomerPreference>(),
                    PromoCodes = new List<CustomerPromoCode>(),
                };
            }

            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;

            customer.Preferences?.Clear();
            if (preferences?.Count() < 1)
                return customer;

            customer.Preferences = preferences
                .Select(x => new CustomerPreference(customer, x))
                .ToList();

            return customer;
        }
    }
}