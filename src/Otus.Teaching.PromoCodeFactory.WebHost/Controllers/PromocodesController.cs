﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> promoCodeRepository;
        private readonly IRepository<Preference> preferenceRepository;
        private readonly IRepository<Customer> customerRepository;

        public PromocodesController(
            IRepository<PromoCode> promoCodeRepository,
            IRepository<Preference> preferenceRepository,
            IRepository<Customer> customerRepository)
        {
            this.promoCodeRepository = promoCodeRepository;
            this.preferenceRepository = preferenceRepository;
            this.customerRepository = customerRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promoCodes = await promoCodeRepository.GetAllAsync();

            if(!promoCodes.Any())
            {
                NotFound();
            }

            var response = promoCodes
                .Select(x => new PromoCodeResponse(x)).ToList();

            return Ok(response);
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        [HttpPost]
        public async Task<ActionResult<PromoCodeShortResponse>> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            if (request == null)
            {
                return BadRequest();
            }

            var preferenceList = await preferenceRepository.GetAllAsync();
            var preference = preferenceList
                .First(x => x.Name == request.Preference);
            if (preference == null)
            {
                return BadRequest();
            }

            var customerList = await customerRepository
                .GetAllAsync();
            customerList = customerList
                .Where(c => c.Preferences
                    .Any(x => x.Preference.Id == preference.Id));

            var promoCode = PromoCodeMapper(request, preference, customerList);

            await promoCodeRepository.CreateAsync(promoCode);

            var response = new PromoCodeShortResponse(promoCode);
            return CreatedAtRoute(null, response);
        }

        private PromoCode PromoCodeMapper(GivePromoCodeRequest request,
                                          Preference preference,
                                          IEnumerable<Customer> customers)
        {
            var promoCode = new PromoCode
            {
                Code = request.PromoCode,
                ServiceInfo = request.ServiceInfo,
                PartnerName = request.PartnerName,
                BeginDate = DateTime.Today,
                EndDate = DateTime.Today.AddDays(30),
                Preference = preference,
                Customers = new List<CustomerPromoCode>(),
            };

            if (customers?.Count() < 1)
            {
                return promoCode;
            }

            promoCode.Customers = customers
                .Select(x => new CustomerPromoCode(promoCode, x))
                .ToList();

            return promoCode;
        }
    }
}