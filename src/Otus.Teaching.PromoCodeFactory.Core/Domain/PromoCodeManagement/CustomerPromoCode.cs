﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class CustomerPromoCode : BaseEntity
    {        
        public Guid CustomerId { get; set; }

        public virtual Customer Customer { get; set; }

        public Guid PromoCodeId { get; set; }

        public virtual PromoCode PromoCode { get; set; }

        public CustomerPromoCode()
        {
        }

        public CustomerPromoCode(PromoCode promoCode, Customer customer)
        {
            PromoCode = promoCode;
            Customer = customer;
        }

    }
}
