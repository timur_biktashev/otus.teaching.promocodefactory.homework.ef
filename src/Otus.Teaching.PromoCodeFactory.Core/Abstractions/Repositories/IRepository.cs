﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        /// <summary>
        /// Получить список записей
        /// </summary>
        /// <returns>Список записей</returns>
        Task<IEnumerable<T>> GetAllAsync();
        
        /// <summary>
        /// Получить запись по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор</param>
        /// <returns>Запись</returns>
        Task<T> GetByIdAsync(Guid id);

        /// <summary>
        /// Создать запись
        /// </summary>
        /// <param name="entity">Сущность</param>
        Task CreateAsync(T entity);

        /// <summary>
        /// Получить список записей по идентифкаторам
        /// </summary>
        /// <param name="guids">Список идентификаторов</param>
        /// <returns>Список записей</returns>
        Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> guids);

        /// <summary>
        /// Удалить запись по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор</param>
        Task DeleteByIdAsync(Guid id);

        /// <summary>
        /// Удалить запись
        /// </summary>
        /// <param name="entity">Сущность для удаления</param>
        Task DeleteAsync(T entity);

        /// <summary>
        /// Обновить запись
        /// </summary>
        /// <param name="entity">Сущность</param>
        Task UpdateAsync(T entity);
    }
}