﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class EfDbInitializer : IDbInitializer
    {
        private readonly DataContext dataContext;

        public EfDbInitializer(DataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public void InitializeDb()
        {
            dataContext.Database.EnsureDeleted();
            dataContext.Database.EnsureCreated();

            dataContext.AddRange(FakeDataFactory.Employees);
            dataContext.SaveChanges();

            dataContext.AddRange(FakeDataFactory.Preferences);
            dataContext.SaveChanges();

            dataContext.AddRange(FakeDataFactory.Customers);
            dataContext.SaveChanges();
        }
    }
}
