﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        private readonly DataContext dataContext;

        public EfRepository(DataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        ///<inheritdoc/>
        public async Task CreateAsync(T entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            await dataContext.Set<T>().AddAsync(entity);
            await dataContext.SaveChangesAsync();
        }

        ///<inheritdoc/>
        public async Task DeleteAsync(T entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            dataContext.Set<T>().Remove(entity);
            await dataContext.SaveChangesAsync();
        }

        ///<inheritdoc/>
        public async Task DeleteByIdAsync(Guid id)
        {
            var entity = await GetByIdAsync(id);
            if (entity == null)
                throw new ArgumentException(nameof(id));
            await dataContext.SaveChangesAsync();
        }

        ///<inheritdoc/>
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var entities = await dataContext.Set<T>().ToListAsync();
            return entities;
        }

        ///<inheritdoc/>
        public async Task<T> GetByIdAsync(Guid id)
        {
            var entity = await dataContext.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
            return entity;
        }

        ///<inheritdoc/>
        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> guids)
        {
            if (guids == null)
                throw new ArgumentNullException(nameof(guids));

            var entities = await dataContext
                .Set<T>()
                .Where(x => guids.Contains(x.Id)).ToListAsync();
            return entities;
        }

        ///<inheritdoc/>
        public async Task UpdateAsync(T entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            dataContext
                .Set<T>()
                .Update(entity);
            await dataContext.SaveChangesAsync();
        }
    }
}
